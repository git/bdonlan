git-fmt-merge-msg(1)
====================

NAME
----
git-fmt-merge-msg - Produce a merge commit message


SYNOPSIS
--------
[verse]
git-fmt-merge-msg [--summary | --no-summary] <$GIT_DIR/FETCH_HEAD
git-fmt-merge-msg [--summary | --no-summary] -F <file>

DESCRIPTION
-----------
Takes the list of merged objects on stdin and produces a suitable
commit message to be used for the merge commit, usually to be
passed as the '<merge-message>' argument of `git-merge`.

This script is intended mostly for internal use by scripts
automatically invoking `git-merge`.

OPTIONS
-------

--summary::
	In addition to branch names, populate the log message with
	one-line descriptions from the actual commits that are being
	merged.

--no-summary::
	Do not list one-line descriptions from the actual commits being
	merged.

--file <file>, -F <file>::
	Take the list of merged objects from <file> instead of
	stdin.

CONFIGURATION
-------------

merge.summary::
	Whether to include summaries of merged commits in newly
	merge commit messages. False by default.

SEE ALSO
--------
linkgit:git-merge[1]


Author
------
Written by Junio C Hamano <junkio@cox.net>

Documentation
--------------
Documentation by Petr Baudis, Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the linkgit:git[7] suite
